
# fpm Build image

## purpose

This project creates a CentOS docker Image with the  [https://github.com/jordansissel/fpm](https://github.com/jordansissel/fpm), fpm a tool to build multiple platforms packages (deb, rpm, ... etc) with great ease and sanity.

## usage

- fpm usage see [Read The Docs](https://fpm.readthedocs.io/en/latest/)

- example usage with GitLab-CI f.i.: [pandoc rpm build](https://gitlab.com/gioxa/rpm-build/pandoc)

----

*Build with [odagrun](https://www.odagrun.com) on openshift-online-starter*

