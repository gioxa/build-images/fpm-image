# ${ODAGRUN_IMAGE_REFNAME}
                
[![](https://images.microbadger.com/badges/version/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/image/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/license/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME} "Get your own license badge on microbadger.com") [![](https://img.shields.io/badge/Gitlab-source-blue.svg)](${CI_PROJECT_URL}) [![](https://img.shields.io/docker/pulls/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://hub.docker.com/r/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME})

## Description
        
docker CentOS ${DISTRO_RELEASE} image with [https://github.com/jordansissel/fpm](https://github.com/jordansissel/fpm).
        
## purpose
        
Image for packaging Software with GitLab-CI

> Effing package management! 
> 
> Build packages for multiple platforms (deb, rpm, etc) with great ease and sanity.
        
## image tags:

Tags are noted as:

`<fpm-version>-<short-build-date>`

when build, all CentOS ${DISTRO_RELEASE} packages will be up to date and fpm will be the latest version.

## Usage

fpm usage see [Read The Docs](https://fpm.readthedocs.io/en/latest/)

example usage with GitLab-CI f.i.: [pandoc rpm build](https://gitlab.com/gioxa/rpm-build/pandoc)

## Image Config

```yaml
$DOCKER_CONFIG_YML
```
## Auto Update

Weekly updated with GitLab scheduled Pipelines

## Content

### baselayer

```bash
install_packages="ruby rubygems rubygem-* rpm-build"
```

### app layer:

```bash
gem install --no-ri --no-rdoc fpm
```

----

*Build with [odagrun](https://www.odagrun.com) on openshift-online-starter*

