stages:
  - build
  - gems
  - test
  - deploy
  - mb
  - cleanup

variables:
  # for auto-labeling META Data of the docker image
  ODAGRUN_IMAGE_LICENSES: MIT
  ODAGRUN_IMAGE_VENDOR: Gioxa Ltd
  ODAGRUN_IMAGE_TITLE: Custom fpm CentOS 7 image to package software with great ease and sanity, build with odagrun.
  ODAGRUN_IMAGE_DOCUMENTATION: https://fpm.readthedocs.io/en/latest/
  ODAGRUN_IMAGE_REFNAME: fpm-centos
  # release of the distro for caches and make_os
  DISTRO_RELEASE: "7"
  DOCKER_NAMESPACE: gioxa
  #DOCKER_CREDENTIALS: xxxxx set in secret variables
  GIT_CACHE_STRATEGY: pull
  ODAGRUN_POD_SIZE: micro
  
# append to base image the base for our application
build_base:
  image: gioxa/imagebuilder-c${DISTRO_RELEASE}
  stage: build
  retry: 1
  variables:
      ODAGRUN_POD_SIZE: medium
      WORK_SPACES: |
          - name: repocache C$DISTRO_RELEASE
            key: x86_64
            scope: global
            path:
              - cache/yum/x86_64/$DISTRO_RELEASE/base
              - cache/yum/x86_64/$DISTRO_RELEASE/updates
            strategy: pull
  script:
    - export target="${CI_PROJECT_DIR}/rootfs"
    - export BASE="${CI_PROJECT_DIR}/"
    - export OS_CONFIG=ruby_base.conf
    - export ODAGRUN_IMAGE_REFNAME=base-fpm-centos
    - make_os
    - registry_push --rootfs=$target --name=$CI_PIPELINE_ID --ISR --reference=base ./*
  tags:
    - odagrun

# append to base image the base for installing our application
build_install:
  image: gioxa/imagebuilder-c${DISTRO_RELEASE}
  stage: build
  retry: 1
  variables:
    ODAGRUN_POD_SIZE: medium
    GIT_CACHE_STRATEGY: push-pull
    WORK_SPACES: |
          - name: repocache C$DISTRO_RELEASE
            key: x86_64
            scope: global
            path:
              - cache/yum/x86_64/$DISTRO_RELEASE/base
              - cache/yum/x86_64/$DISTRO_RELEASE/updates
            strategy: push-pull
            threshold:
              path:
                - cache/yum/x86_64/$DISTRO_RELEASE/base/packages/*.rpm
                - cache/yum/x86_64/$DISTRO_RELEASE/updates/packages/*.rpm
  script:
    - export target=${CI_PROJECT_DIR}/rootfs
    - export BASE=${CI_PROJECT_DIR}/
    - export OS_CONFIG=ruby_install.conf
    - export ODAGRUN_IMAGE_REFNAME=install-fpm-centos
    - make_os
    - registry_push --rootfs=$target --name=$CI_PIPELINE_ID --ISR --reference=install  .
  tags:
    - odagrun

gem_install:
  image: ImageStream/$CI_PIPELINE_ID:install
  stage: gems
  retry: 1
  variables:
    ODAGRUN_POD_SIZE: medium
    WORK_SPACES: |
          - name: dock_info
            strategy: push
            environment: ODAGRUN_IMAGE_VERSION,ODAGRUN_IMAGE_DESCRIPTION,ODAGRUN_IMAGE_REFNAME
          - name: gemcache
            scope: global
            path: /builds/.gem/ruby/cache
            strategy: push-pull
            threshold:
              path: >-
                       /builds/.gem/ruby/cache/*.gem
  script:
    - gem install --no-ri --no-rdoc fpm
    - ../bin/fpm --version
    - export ODAGRUN_IMAGE_VERSION=$(../bin/fpm --version)
    # add date stamp of build to version
    - copy --from_text="-${ODAGRUN_SHORT_DATE}" --append --to_var=ODAGRUN_IMAGE_VERSION
    - copy --from_file=./docker_config.yml --to_var=DOCKER_CONFIG_YML
    - copy --from_file=./image_description.md --to_var=ODAGRUN_IMAGE_DESCRIPTION --substitute
    - >-
         registry_push
         --from_ISR --from_name=$CI_PIPELINE_ID --from_reference=base
         --rootfs=/ 
         --ISR --name=$CI_PIPELINE_ID --reference=fpm
         --config 
         /builds/bin /builds/.gem
  tags:
    - odagrun

test_fpm:
  image: ImageStream/$CI_PIPELINE_ID:fpm
  stage: test
  dependencies: []
  variables:
    GIT_STRATEGY: none
  script:
    - echo $PATH
    - fpm --version
  tags:
    - odagrun
 
# push image to registry with tag and tag latest
push-image-tags:
  stage: deploy
  image: scratch
  retry: 1
  variables:
    GIT_STRATEGY: none
    WORK_SPACES: |
                   - name: dock_info
                     strategy: pull
  script:
     - copy --from_text="to docker image--> ${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}:${ODAGRUN_IMAGE_VERSION}" --to_term --quiet
     - >-
        DockerHub_set_description
        --allow-fail
        --image=$DOCKER_NAMESPACE/$ODAGRUN_IMAGE_REFNAME
        --set-private=no
     - >-
        registry_push
        --from_ISR --from_name=$CI_PIPELINE_ID --from_reference=fpm 
        --image="${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}:${ODAGRUN_IMAGE_VERSION}"
        --skip_label
     - >-
        registry_tag_image
        --image="${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}:${ODAGRUN_IMAGE_VERSION}"
        --tag=latest
     - MicroBadger_Update  --allow-fail --image="${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}"
  only:
    - master
  tags:
     - odagrun
     
# review push image to registry with tag
image-review-branch:
  stage: deploy
  image: scratch
  variables:
    GIT_STRATEGY: none
  dependencies: []
  environment:
    name: review/$CI_BUILD_REF_NAME
    on_stop: stop_review
  script:
     - registry_push --from_ISR --from_name=$CI_PIPELINE_ID --from_reference=fpm --image=$DOCKER_NAMESPACE/$ODAGRUN_IMAGE_REFNAME:$CI_ENVIRONMENT_SLUG --skip_label
  only:
    - branches
  except:
    - tags
    - master
  tags:
     - odagrun

stop_review:
  stage: deploy
  dependencies: []
  image: scratch
  script:
    - DockerHub_delete_tag --image=$DOCKER_NAMESPACE/$ODAGRUN_IMAGE_REFNAME --tag=$CI_ENVIRONMENT_SLUG --allow-fail
  variables:
    GIT_STRATEGY: none
    GIT_CACHE_STRATEGY: clean
  when: manual
  environment:
    name: review/$CI_BUILD_REF_NAME
    action: stop
  only:
    - branches
  except:
    - tags
    - master
  tags:
    - odagrun

# test if our pushed tag-image is availleble
cleanup:
  image: scratch
  dependencies: []
  variables:
    GIT_STRATEGY: none
    WORK_SPACES: |
                  - name: dock_info
                    strategy: clean
  script:
   - ImageStream_delete --name=$CI_PIPELINE_ID --allow-fail
   - cd
  stage: cleanup
  allow_failure: true
  when: always
  tags:
     - odagrun
